MAINF := gitcomposer
PARSEF := gitcomposer-parse
COMPPATH := /etc/bash_completion.d/$(MAINF)
CURPATH := $(shell pwd)

install:
		@echo -----------------
		@echo ---- INSTALL ----
		@echo -----------------
		ln -s $(CURPATH)/$(MAINF) $(COMPPATH)
		ln -s $(CURPATH)/$(PARSEF) /usr/local/bin/_$(PARSEF)
		@echo ------------------
		@echo ---- COMPLETE ----
		@echo ------------------

clean:
	@echo ------------------
	@echo ---- CLEANING ----
	@echo ------------------
	rm -r $(COMPPATH)
	rm -r /usr/local/bin/_$(PARSEF)
	@echo ------------------
	@echo ---- COMPLETE ----
	@echo ------------------
